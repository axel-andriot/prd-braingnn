# -*- coding: utf-8 -*-
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import Tooltip as ttp
from tkinter import PhotoImage
import process_data


def runProcessData():
    global rootDirectory,atlasProcessData,seedNumber,classNumber
    rootDirectoryString=rootDirectory.get()
    atlasProcessDataString=atlasProcessData.get()
    seedNumberString=seedNumber.get()
    classNumberString=classNumber.get()
    process_data.main(rootDirectoryString,atlasProcessDataString,seedNumberString,classNumberString)


def getDirectoryDataRoot():
    global rootDirectory  
    rootDirectory.set( filedialog.askdirectory(initialdir="C:/",title="Choose Directory"))
  
    
def main(notebook):
    global rootDirectory,atlasProcessData,seedNumber,classNumber
    ##########################################################################################################
    #Onglet Process Data
    ##########################################################################################################

    ongletProcessData = ttk.Frame(notebook)       
    ongletProcessData.pack()
    notebook.add(ongletProcessData, text='Process Data') 

    ############################## 
    #DirectoryRoot
    ##############################
    labelFrameDirectoryRoot = LabelFrame(ongletProcessData, text="DirectoryRoot", padx=20, pady=20)
    labelFrameDirectoryRoot.pack(fill="both", expand="yes")

    rootDirectory=StringVar()
    Label(labelFrameDirectoryRoot, text="Choose your directory where your data are. WARNING: The file 'Subject_ID' must be in it.").pack()
    Entry(labelFrameDirectoryRoot, textvariable=rootDirectory, width=65).pack(side='left')
    buttonBrowseFetchData=Button(labelFrameDirectoryRoot, text='Browse', command = getDirectoryDataRoot).pack(side='right',pady=20)
    

    ############################## 
    #AtlasOption
    ##############################
    labelFrameAtlas = LabelFrame(ongletProcessData, text="Atlas", padx=20, pady=20)
    labelFrameAtlas.pack(fill="both", expand="yes")
     
    Label(labelFrameAtlas, text="Brain parcellation atlas. (Default: cc200)").pack()

    vals = ['ho', 'cc200', 'cc400']
    etiqs = ['ho', 'cc200', 'cc400']
    atlasProcessData = StringVar()
    atlasProcessData.set(vals[1])
    for i in range(3):
        b = Radiobutton(labelFrameAtlas, variable=atlasProcessData, text=etiqs[i], value=vals[i])
        b.pack(side='left', expand=1)   
    
    ############################## 
    #SeedOption
    ##############################
    labelFrameSeed = LabelFrame(ongletProcessData, text="Seed", padx=20, pady=20)
    labelFrameSeed.pack(fill="both", expand="yes")
     
    Label(labelFrameSeed, text="Seed for random initialisation. (Default: 1234)").pack()
    seedNumber=IntVar()
    seedNumber.set(1234)
    Spinbox(labelFrameSeed, from_=0,textvariable=seedNumber).pack() 

    
    ############################## 
    #ClassNumberOption
    ##############################
    labelFrameClassNumber = LabelFrame(ongletProcessData, text="ClassNumber", padx=20, pady=20)
    labelFrameClassNumber.pack(fill="both", expand="yes")
     
    Label(labelFrameClassNumber, text="Number of classes. (Default:2)").pack()
    classNumber=IntVar()
    classNumber.set(2)
    Spinbox(labelFrameClassNumber, from_=0,to=10,textvariable=classNumber).pack() 

   
    
    ############################## 
    #RunProcessData + ProgressBar
    ##############################

    Button(ongletProcessData, text = 'Start', command = runProcessData,width=10).pack() 

   
    
