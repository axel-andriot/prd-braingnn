import os
import numpy as np
import argparse
import time
import copy

import torch
import torch.nn.functional as F
from torch.optim import lr_scheduler
from tensorboardX import SummaryWriter

from imports.ABIDEDataset import ABIDEDataset
from torch_geometric.data import DataLoader
from net.braingnn import Network
from imports.utils import train_val_test_split
from sklearn.metrics import classification_report, confusion_matrix

 
      

def init_param(epochNumberParam,epochStartingParam,sizeBatchParam,foldParam,learningRateParam,stepSizeParam,gammaParam,weightDecayParam,lamb0Param,lamb1Param,lamb2Param,lamb3Param,lamb4Param,lamb5Param,layerNumberParam,poolingRatioParam,featureDimParam,roiNumberParam,classNumberParam,optimParam,rootDirectoryParam,save_modelParam,load_modelParam):
      global epochNumber,epochStarting,sizeBatch,fold,learningRate,stepSize,gamma,weightDecay,lamb0,lamb1,lamb2,lamb3,lamb4,lamb5,layerNumber,poolingRatio,featureDim,roiNumber,classNumber,optim,rootDirectory,writer,save_model,load_model,save_path
      epochNumber=epochNumberParam.get()
      epochStarting=epochStartingParam.get()
      sizeBatch=sizeBatchParam.get()
      fold=foldParam.get()
      learningRate=learningRateParam.get()   
      stepSize=stepSizeParam.get()  
      gamma=gammaParam.get()  
      weightDecay=weightDecayParam.get()    
      lamb0=lamb0Param.get() 
      lamb1=lamb1Param.get() 
      lamb2=lamb2Param.get()  
      lamb3=lamb3Param.get()
      lamb4=lamb4Param.get() 
      lamb5=lamb5Param.get()  
      layerNumber=layerNumberParam.get()  
      poolingRatio=poolingRatioParam.get()   
      featureDim=featureDimParam.get() 
      roiNumber=roiNumberParam.get() 
      classNumber=classNumberParam.get()  
      optim=optimParam.get()
      rootDirectory=rootDirectoryParam.get()
      save_model=save_modelParam.get()
      load_model=load_modelParam.get()
      save_path='./model/'
      writer = SummaryWriter(os.path.join('./log',str(fold)))
      

def gnn_run():
    global EPS,train_loader,val_loader,test_loader,device,rootDirectory,sizeBatch,fold,featureDim,poolingRatio,classNumber,optim,layerNumber,weightDecay,gamma,stepSize,scheduler,optimizer,epochStarting,epochNumber,model,device,writer,save_model,load_model,save_path,train_dataset,val_dataset,test_dataset
    torch.manual_seed(123)
    name = 'ABIDE'
    EPS = 1e-10
    #device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    device = torch.device("cpu")
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    ################## Define Dataloader ##################################
    
    dataset = ABIDEDataset(rootDirectory,name)
    dataset.data.y = dataset.data.y.squeeze()
    dataset.data.x[dataset.data.x == float('inf')] = 0
    
    
    
    tr_index,val_index,te_index = train_val_test_split(fold=fold)
    
    #print(tr_index,type(tr_index))
    train_dataset = dataset[tr_index.astype(np.int64)]
    val_dataset = dataset[val_index.astype(np.int64)]
    test_dataset = dataset[te_index.astype(np.int64)]
    
    
    train_loader = DataLoader(train_dataset,sizeBatch, shuffle= True)
    val_loader = DataLoader(val_dataset, sizeBatch, shuffle=False)
    test_loader = DataLoader(test_dataset, sizeBatch, shuffle=False)
    
    
    print('Dataset created')
    ############### Define Graph Deep Learning Network ##########################
    model = Network(featureDim,poolingRatio,classNumber).to(device)
    #print(model)
    
    if optim == 'Adam':
        optimizer = torch.optim.Adam(model.parameters(), lr=layerNumber, weight_decay=weightDecay)
    elif optim == 'SGD':
        optimizer = torch.optim.SGD(model.parameters(), lr=layerNumber , momentum = 0.9, weight_decay=weightDecay, nesterov = True)
    
    scheduler = lr_scheduler.StepLR(optimizer, stepSize, gamma)
    print('scheduler ok')
    #######################################################################################
    ############################   Model Training #########################################
    #######################################################################################
    best_model_wts = copy.deepcopy(model.state_dict())
    best_loss = 1e10
    for epoch in range(0, epochNumber):
        since  = time.time()
        tr_loss, s1_arr, s2_arr, w1, w2 = train(epoch)
        tr_acc = test_acc(train_loader)
        val_acc = test_acc(val_loader)
        val_loss = test_loss(val_loader,epoch)
        time_elapsed = time.time() - since
        print('*====**')
        print('{:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
        print('Epoch: {:03d}, Train Loss: {:.7f}, '
              'Train Acc: {:.7f}, Test Loss: {:.7f}, Test Acc: {:.7f}'.format(epoch, tr_loss,
                                                           tr_acc, val_loss, val_acc))
        
        writer.add_scalars('Acc',{'train_acc':tr_acc,'val_acc':val_acc},  epoch)
        writer.add_scalars('Loss', {'train_loss': tr_loss, 'val_loss': val_loss},  epoch)
        writer.add_histogram('Hist/hist_s1', s1_arr, epoch)
        writer.add_histogram('Hist/hist_s2', s2_arr, epoch)
        
        if val_loss < best_loss and epoch > 1:
            print("saving best model")
            best_loss = val_loss
            best_model_wts = copy.deepcopy(model.state_dict())
            
            if save_model:
                torch.save(best_model_wts, os.path.join(save_path,str(fold)+'.pth'))
    writer.close()         
    #######################################################################################
    ######################### Testing on testing set ######################################
    #######################################################################################
    
    if load_model:
        model = Network(featureDim,poolingRatio,classNumber).to(device)
        model.load_state_dict(torch.load(os.path.join(save_path,str(fold)+'.pth')))
        model.eval()
        preds = []
        correct = 0
        for data in val_loader:
            data = data.to(device)
            outputs= model(data.x, data.edge_index, data.batch, data.edge_attr,data.pos)
            pred = outputs[0].max(1)[1]
            preds.append(pred.cpu().detach().numpy())
            correct += pred.eq(data.y).sum().item()
        preds = np.concatenate(preds,axis=0)
        trues = val_dataset.data.y.cpu().detach().numpy()
        cm = confusion_matrix(trues,preds)
        print("Confusion matrix")
        print(classification_report(trues, preds))

    else:
       model.load_state_dict(best_model_wts)
       model.eval()
       test_accuracy = test_acc(test_loader)
       test_l= test_loss(test_loader,0)
       print("===========================")
       print("Test Acc: {:.7f}, Test Loss: {:.7f} ".format(test_accuracy, test_l))
       


############################### Define Other Loss Functions ########################################
def topk_loss(s,ratio):
    global EPS  
    if ratio > 0.5:
        ratio = 1-ratio
    s = s.sort(dim=1).values
    res =  -torch.log(s[:,-int(s.size(1)*ratio):]+EPS).mean() -torch.log(1-s[:,:int(s.size(1)*ratio)]+EPS).mean()
    return res


def consist_loss(s):
    global device
    if len(s) == 0:
        return 0
    s = torch.sigmoid(s)
    W = torch.ones(s.shape[0],s.shape[0])
    D = torch.eye(s.shape[0])*torch.sum(W,dim=1)
    L = D-W
    L = L.to(device)
    res = torch.trace(torch.transpose(s,0,1) @ L @ s)/(s.shape[0]*s.shape[0])
    return res

###################### Network Training Function#####################################
def train(epoch):
    global scheduler,optimizer,poolingRatio,classNumber,lamb0,lamb1,lamb2,lamb3,lamb4,lamb5,train_dataset,model,writer,train_loader,device
    print('train...........')
    scheduler.step()
    '''
    for param_group in optimizer.param_groups:
        print("LR", param_group['lr'])
    '''
    model.train()
    s1_list = []
    s2_list = []
    loss_all = 0
    step = 0
    for data in train_loader:
        data = data.to(device)
        optimizer.zero_grad()
        output, w1, w2, s1, s2 = model(data.x, data.edge_index, data.batch, data.edge_attr, data.pos)
        s1_list.append(s1.view(-1).detach().cpu().numpy())
        s2_list.append(s2.view(-1).detach().cpu().numpy())

        loss_c = F.nll_loss(output, data.y)
        
        loss_p1 = (torch.norm(w1, p=2)-1) ** 2
        loss_p2 = (torch.norm(w2, p=2)-1) ** 2
        loss_tpk1 = topk_loss(s1,poolingRatio)
        loss_tpk2 = topk_loss(s2,poolingRatio)
        loss_consist = 0
        for c in range(classNumber):
            loss_consist += consist_loss(s1[data.y == c])
        loss = lamb0*loss_c + lamb1 * loss_p1 + lamb2 * loss_p2 \
                   + lamb3 * loss_tpk1 + lamb4 *loss_tpk2 + lamb5* loss_consist
        
        writer.add_scalar('train/classification_loss', loss_c, epoch*len(train_loader)+step)
        writer.add_scalar('train/unit_loss1', loss_p1, epoch*len(train_loader)+step)
        writer.add_scalar('train/unit_loss2', loss_p2, epoch*len(train_loader)+step)
        writer.add_scalar('train/TopK_loss1', loss_tpk1, epoch*len(train_loader)+step)
        writer.add_scalar('train/TopK_loss2', loss_tpk2, epoch*len(train_loader)+step)
        writer.add_scalar('train/GCL_loss', loss_consist, epoch*len(train_loader)+step)
        
        step = step + 1

        loss.backward()
        loss_all += loss.item() * data.num_graphs
        optimizer.step()

        s1_arr = np.hstack(s1_list)
        s2_arr = np.hstack(s2_list)
    return loss_all / len(train_dataset), s1_arr, s2_arr ,w1,w2


###################### Network Testing Function#####################################
def test_acc(loader):
    global device,model
    model.eval()
    correct = 0
    for data in loader:
        data = data.to(device)
        outputs= model(data.x, data.edge_index, data.batch, data.edge_attr,data.pos)
        pred = outputs[0].max(dim=1)[1]
        correct += pred.eq(data.y).sum().item()

    return correct / len(loader.dataset)

def test_loss(loader,epoch):
    global device,poolingRatio,classNumber,lamb0,lamb1,lamb2,lamb3,lamb4,lamb5,model
    print('testing...........')
    model.eval()
    loss_all = 0
    for data in loader:      
        data = data.to(device)
        output, w1, w2, s1, s2= model(data.x, data.edge_index, data.batch, data.edge_attr,data.pos)        
        loss_c = F.nll_loss(output, data.y)
        
        loss_p1 = (torch.norm(w1, p=2)-1) ** 2
        loss_p2 = (torch.norm(w2, p=2)-1) ** 2
        loss_tpk1 = topk_loss(s1,poolingRatio)
        loss_tpk2 = topk_loss(s2,poolingRatio)
        loss_consist = 0 
        for c in range(classNumber):
            loss_consist += consist_loss(s1[data.y == c])
        loss = lamb0*loss_c + lamb1 * loss_p1 + lamb2 * loss_p2 + lamb3 * loss_tpk1 + lamb4 *loss_tpk2 + lamb5* loss_consist

        loss_all += loss.item() * data.num_graphs
       
    return loss_all / len(loader.dataset)

