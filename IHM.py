# -*- coding: utf-8 -*-



## Onglets

from tkinter import *
from tkinter import ttk
import OngletFetchData
import OngletProcessData
import OngletGNN



##########################################################################################################
#IHM
##########################################################################################################

fenetre = Tk()

notebook = ttk.Notebook(fenetre)   # Création du système d'onglets
notebook.pack()

#Onglet Fetch Data
OngletFetchData.main(notebook)

#Onglet Process Data
OngletProcessData.main(notebook)

#Onglet GNN
OngletGNN.main(notebook)
  

fenetre.mainloop()


