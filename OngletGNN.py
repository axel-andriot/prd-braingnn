# -*- coding: utf-8 -*-
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import Tooltip as ttp
from tkinter import PhotoImage
import gnn_main


def gnn_start():
    global epochNumber,epochStarting,sizeBatch,fold,learningRate,stepSize,gamma,weightDecay,lamb0,lamb1,lamb2,lamb3,lamb4,lamb5,layerNumber,poolingRatio,featureDim,roiNumber,classNumber,optim,rootDirectory,save_model,load_model
    gnn_main.init_param(epochNumber,epochStarting,sizeBatch,fold,learningRate,stepSize,gamma,weightDecay,lamb0,lamb1,lamb2,lamb3,lamb4,lamb5,layerNumber,poolingRatio,featureDim,roiNumber,classNumber,optim,rootDirectory,save_model,load_model)
    gnn_main.gnn_run()
    
    

def getDirectoryDataRoot():
    global rootDirectory  
    rootDirectory.set( filedialog.askdirectory(initialdir="C:/",title="Choose Directory"))
  
    
def main(notebook):
    global epochNumber,epochStarting,sizeBatch,fold,learningRate,stepSize,gamma,weightDecay,lamb0,lamb1,lamb2,lamb3,lamb4,lamb5,layerNumber,poolingRatio,featureDim,roiNumber,classNumber,optim,rootDirectory,save_model,load_model
    ##########################################################################################################
    #Onglet Process Data
    ##########################################################################################################

    ongletGNN = ttk.Frame(notebook)       
    ongletGNN.pack()
    notebook.add(ongletGNN, text='GNN') 

    ############################## 
    #DirectoryRoot
    ##############################
    labelFrameDirectoryRoot = LabelFrame(ongletGNN, text="DirectoryRoot", padx=20, pady=20)
    labelFrameDirectoryRoot.pack(fill="both", expand="yes")

    rootDirectory=StringVar()
    Label(labelFrameDirectoryRoot, text="Choose your directory where your data are. WARNING: It must be './data/ABIDE_pcp/cpac/filt_noglobal'.").pack()
    Entry(labelFrameDirectoryRoot, textvariable=rootDirectory, width=65).pack(side='left')
    buttonBrowseFetchData=Button(labelFrameDirectoryRoot, text='Browse', command = getDirectoryDataRoot).pack(side='right',pady=20)
    

    ############################## 
    #GNNParameter
    ##############################
    labelFrameGNNParameter = LabelFrame(ongletGNN, text="GNN Parameter", padx=20, pady=20)
    labelFrameGNNParameter.pack(fill="both", expand="yes")

     
    Label(labelFrameGNNParameter, text="number of epochs of training: (Default:100)").grid(row=0)
    epochNumber=IntVar()
    epochNumber.set(100)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=epochNumber).grid(row=0,column=1)
    
    Label(labelFrameGNNParameter, text="starting epoch: (Default:0)").grid(row=1)
    epochStarting=IntVar()
    epochStarting.set(0)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=epochStarting).grid(row=1,column=1)
    
    Label(labelFrameGNNParameter, text="size of the batches: (Default:100)").grid(row=2)
    sizeBatch=IntVar()
    sizeBatch.set(100)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=sizeBatch).grid(row=2,column=1)
    
    Label(labelFrameGNNParameter, text="training which fold: (Default:0)").grid(row=3)
    fold=IntVar()
    fold.set(0)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=fold).grid(row=3,column=1)
    
    Label(labelFrameGNNParameter, text="learning rate: (Default:0.01)").grid(row=4)
    learningRate=DoubleVar()
    learningRate.set(0.01)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=learningRate).grid(row=4,column=1)
    
    Label(labelFrameGNNParameter, text="scheduler step size: (Default:20)").grid(row=5)
    stepSize=IntVar()
    stepSize.set(20)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=stepSize).grid(row=5,column=1)
    
    Label(labelFrameGNNParameter, text="scheduler shrinking rate, gamma: (Default:0.5)").grid(row=6)
    gamma=DoubleVar()
    gamma.set(0.5)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=gamma).grid(row=6,column=1)
    
    Label(labelFrameGNNParameter, text="regularization, weight decay: (Default:5e-3)").grid(row=1,column=4)
    weightDecay=DoubleVar()
    weightDecay.set(5e-3)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=weightDecay).grid(row=1,column=5)
    
    Label(labelFrameGNNParameter, text="classification loss weight, lamb0: (Default:1)").grid(row=2,column=4)
    lamb0=DoubleVar()
    lamb0.set(1)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=lamb0).grid(row=2,column=5)
    
    Label(labelFrameGNNParameter, text="s1 unit regularization, lamb1: (Default:0)").grid(row=3,column=4)
    lamb1=DoubleVar()
    lamb1.set(0)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=lamb1).grid(row=3,column=5)
    
    Label(labelFrameGNNParameter, text="s2 unit regularization, lamb2: (Default:0)").grid(row=4,column=4)
    lamb2=DoubleVar()
    lamb2.set(0)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=lamb2).grid(row=4,column=5)
    
    Label(labelFrameGNNParameter, text="s1 entropy regularization, lamb3: (Default:0.1)").grid(row=5,column=4)
    lamb3=DoubleVar()
    lamb3.set(0.1)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=lamb3).grid(row=5,column=5)
    
    Label(labelFrameGNNParameter, text="s2 entropy regularization, lamb4: (Default:0.1)").grid(row=6,column=4)
    lamb4=DoubleVar()
    lamb4.set(0.1)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=lamb4).grid(row=6,column=5)
    
    Label(labelFrameGNNParameter, text="s1 consistence regularization, lamb5: (Default:0.1)").grid(row=7,column=4)
    lamb5=DoubleVar()
    lamb5.set(0.1)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=lamb5).grid(row=7,column=5)
    
    Label(labelFrameGNNParameter, text="number of GNN layers: (Default:2)").grid(row=7)
    layerNumber=IntVar()
    layerNumber.set(2)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=layerNumber).grid(row=7,column=1)
    
    Label(labelFrameGNNParameter, text="pooling ratio: (Default:0.5)").grid(row=8)
    poolingRatio=DoubleVar()
    poolingRatio.set(0.5)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=poolingRatio).grid(row=8,column=1)
    
    Label(labelFrameGNNParameter, text="feature dim: (Default:200)").grid(row=9)
    featureDim=IntVar()
    featureDim.set(200)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=featureDim).grid(row=9,column=1)
    
    Label(labelFrameGNNParameter, text="num of ROIs: (Default:200)").grid(row=10)
    roiNumber=IntVar()
    roiNumber.set(200)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=roiNumber).grid(row=10,column=1)
    
    Label(labelFrameGNNParameter, text="num of classes: (Default:2)").grid(row=11)
    classNumber=IntVar()
    classNumber.set(2)
    Spinbox(labelFrameGNNParameter, from_=0,textvariable=classNumber).grid(row=11,column=1)
    
    Label(labelFrameGNNParameter, text="optimization method: SGD, Adam: (Default:Adam)").grid(row=12)
    optim=StringVar()
    optim.set('Adam')
    Entry(labelFrameGNNParameter, textvariable=optim, width=10).grid(row=12,column=1)
    
    ############################## 
    #Save/Load
    ##############################
    labelFrameSaveLoad = LabelFrame(ongletGNN, text="Save/Load", padx=20, pady=20)
    labelFrameSaveLoad.pack(fill="both", expand="yes")

    save_model=BooleanVar()
    save_model.set(True)
    Checkbutton(labelFrameSaveLoad,text='Save:',var=save_model).pack()
    
    load_model=BooleanVar()
    load_model.set(False)
    Checkbutton(labelFrameSaveLoad,text='Load:', var=load_model).pack()
    
    
    
    ############################## 
    #RunProcessData + ProgressBar
    ##############################

    Button(ongletGNN, text = 'Start', command = gnn_start,width=10).pack() 

 