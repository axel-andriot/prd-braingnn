# -*- coding: utf-8 -*-
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import Tooltip as ttp
from tkinter import PhotoImage
import fetch_data


def runFetchData():
    global rootDirectory,pipelineFetchData,atlasFetchData,dowloadFetchData
    rootDirectoryString=rootDirectory.get()
    pipelineFetchDataString=pipelineFetchData.get()
    atlasFetchDataString=atlasFetchData.get()
    dowloadFetchDataString=dowloadFetchData.get()
    fetch_data.main(pipelineFetchDataString,atlasFetchDataString,dowloadFetchDataString,rootDirectoryString)
    


def getDirectoryDataRoot():
    global rootDirectory
    rootDirectory.set(filedialog.askdirectory(initialdir="C:/",title="Choose Directory"))
    
  
    
def main(notebook):
    global rootDirectory,pipelineFetchData,atlasFetchData,dowloadFetchData
    ##########################################################################################################
    #Onglet Fetch Data
    ##########################################################################################################
    
    ongletFetchData = ttk.Frame(notebook)       
    ongletFetchData.pack()
    notebook.add(ongletFetchData, text='Fetch Data') 
    #imgInfo=PhotoImage(file='Image/imageInfo.png')
    #icone=Label(ongletFetchData,image=imgInfo).pack()
    #ttp.CreateToolTip(icone,text="Download ABIDE data and compute functional connectivity matrices")
    
    ############################## 
    #PipelineOption
    ##############################
    labelFramePipline = LabelFrame(ongletFetchData, text="Pipeline", padx=20, pady=20)
    labelFramePipline.pack(fill="both", expand="yes")
     
    Label(labelFramePipline, text="Pipeline to preprocess ABIDE data. (Default: cpac)").pack()
    
    valsPipline = ['ccs', 'cpac', 'dparsf','niak']
    etiqsPipline = ['ccs', 'cpac', 'dparsf','niak']
    pipelineFetchData = StringVar()
    pipelineFetchData.set(valsPipline[1])
    for i in range(4):
        if (i==1):
            b = Radiobutton(labelFramePipline, variable=pipelineFetchData, text=etiqsPipline[i], value=valsPipline[i])
            b.select()
            b.pack(side='left', expand=1)
        else:
            b = Radiobutton(labelFramePipline, variable=pipelineFetchData, text=etiqsPipline[i], value=valsPipline[i])
            #b.deselect()
            b.pack(side='left', expand=1)
        
    ############################## 
    #AtlasOption
    ##############################
    labelFrameAtlas = LabelFrame(ongletFetchData, text="Atlas", padx=20, pady=20)
    labelFrameAtlas.pack(fill="both", expand="yes")
     
    Label(labelFrameAtlas, text="Brain parcellation atlas. (Default: cc200)").pack()
    
    valsAtlas = ['ho', 'cc200', 'cc400']
    etiqsAtlas = ['ho', 'cc200', 'cc400']
    atlasFetchData = StringVar()
    atlasFetchData.set(valsAtlas[1])
    for i in range(3):
        b = Radiobutton(labelFrameAtlas, variable=atlasFetchData, text=etiqsAtlas[i], value=valsAtlas[i])
        b.pack(side='left', expand=1)
    
    ############################## 
    #DownloadOption
    ##############################
    labelFrameDownload = LabelFrame(ongletFetchData, text="Download", padx=20, pady=20)
    labelFrameDownload.pack(fill="both", expand="yes")
     
    Label(labelFrameDownload, text="Dowload data or just compute functional connectivity. (Default: True)").pack()
    
    valsDownload = ['True', 'False']
    etiqsDownload = ['True', 'False']
    dowloadFetchData = StringVar()
    dowloadFetchData.set(valsDownload[0])
    for i in range(2):
        b = Radiobutton(labelFrameDownload, variable=dowloadFetchData, text=etiqsDownload[i], value=valsDownload[i])
        b.pack(side='left', expand=1)
    
    
    ############################## 
    #DirectoryRoot
    ##############################
    labelFrameDirectoryRoot = LabelFrame(ongletFetchData, text="DirectoryRoot", padx=20, pady=20)
    labelFrameDirectoryRoot.pack(fill="both", expand="yes")  
    rootDirectory=StringVar()
    Label(labelFrameDirectoryRoot, text="Choose your directory where your data are. WARNING: The file 'Subject_ID' must be in it.").pack()
    Entry(labelFrameDirectoryRoot, textvariable=rootDirectory, width=65).pack(side='left')
    buttonBrowseFetchData=Button(labelFrameDirectoryRoot, text='Browse', command = getDirectoryDataRoot).pack(side='right',pady=20)
    
    
    ############################## 
    #RunFetchData + ProgressBar
    ##############################

    Button(ongletFetchData, text = 'Start', command = runFetchData,width=10).pack() 
    

    
